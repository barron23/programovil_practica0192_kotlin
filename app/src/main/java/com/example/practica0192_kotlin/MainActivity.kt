package com.example.practica0192_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnPulsar: Button
    private lateinit var btnBorrar: Button
    private lateinit var btnTerminar: Button
    private lateinit var txtNombre: EditText
    private lateinit var lblSaludar: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar los objetos
        btnPulsar = findViewById(R.id.btnSaludar)
        btnBorrar = findViewById(R.id.btnLimpiar)
        btnTerminar = findViewById(R.id.btnCerrar)
        txtNombre = findViewById(R.id.txtNombre)
        lblSaludar = findViewById(R.id.lblSaludo)

        // Codificar el evento clic del botón Pulsar
        btnPulsar.setOnClickListener {
            // Validar si el nombre está vacío
            if (txtNombre.text.toString().isEmpty()) {
                Toast.makeText(this@MainActivity, "Faltó capturar información", Toast.LENGTH_LONG).show()
            } else {
                val str = "Hola ${txtNombre.text.toString()} ¿Cómo estás?"
                lblSaludar.text = str
            }
        }

        // Botón Borrar
        btnBorrar.setOnClickListener {
            // Validar si el nombre está vacío
            if (txtNombre.text.toString().isEmpty()) {
                Toast.makeText(this@MainActivity, "No hay algo que borrar", Toast.LENGTH_LONG).show()
            } else {
                txtNombre.setText("")
                lblSaludar.text = ":: ::"
            }
        }

        // Botón Cerrar
        btnTerminar.setOnClickListener {
            finish()
        }
    }
}